FROM alpine:3.7

RUN apk --update --no-cache add zlib pcre

COPY mnt /mnt

WORKDIR /mnt

CMD sbin/nginx -g 'daemon off;'
