#!/bin/bash

apk add zlib zlib-dev
wget https://nginx.org/download/nginx-1.13.9.tar.gz
tar xf nginx-1.13.9.tar.gz
cd nginx-1.13.9
pwd

git clone https://github.com/vozlt/nginx-module-vts
./configure --add-module=nginx-module-vts --prefix=/mnt
make
make install
cd ..
pwd
mv /mnt mnt
